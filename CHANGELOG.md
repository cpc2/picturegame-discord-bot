# 1.13.1

## Improvements
* Revert change to relative time display
* Enable !rank to fall back to a username, and !stats to fall back to a rank

# 1.13.0

## Features
* Revamped subscription system, adding `!sub host` and `!notify` - see the [guide page](/doc/subscriptions.md)

## Improvements
* Using Discord's built-in relative time display for times in output for !c and when commands are on cooldown
* Make celebrate gifs configurable

# 1.12.1

## Improvements
* Prevent users from using subscriptions if the bot is unable to DM them

## Bug Fixes
* Fix bot removing its own reactions in DMs

# 1.12.0

## Improvements
* Update Discord.js to version 12
* Update dependencies to work with nodejs 14

# 1.11.0

## Features
* Bot can now post log messages to discord channels as specified in config
    * This includes messages from the Reddit bot and from the Discord bot

# 1.10.0

## Improvements
* Cleanup subscriptions to archived rounds when the bot starts
* Prevent subscribing to archived rounds
* Logging improvements
* Inform users of "!sub auto" when they subscribe to their own rounds
* Start !c cooldown when the round starts
* Improve error handling

## Bug Fixes
* Fix issue with future subscriptions when the subscribed round is deleted and reposted
* Don't try to respond to messages consisting of the command prefix repeated
* Update cached data after round corrections

# 1.9.5

## Improvements
* Link to the PictureGame UI instead of the leaderboard wiki when too many stats are requested

# 1.9.4

## Improvements
* Improve stability

# 1.9.3

## Improvements
* Remove some dependencies to make the bot lighter
* Prevent link preview on the Reddit links on new round/round over messages

# 1.9.0

## Features
* Add durations to !c and round over announcements
* Announce in-progress and completed round corrections when notified by the Reddit Bot

## Improvements
* Use the PictureGame API for initialization
* Remove !correct command

## Bug Fixes
* Properly escape underscores in modmail author names

# 1.8.0

## Features
* Announce new modmail messages in the moderator channel

## Improvements
* Display version info for all PG services in !about

# 1.7.4

## Improvements
* Improve error handling and logging

# 1.7.3

## Bug Fixes
* Fix an issue caused by passing a mutable set by reference

# 1.7.2

## Features
* Add support for subscribing to future rounds

## Improvements
* Improve error handling
* Don't notify players about their own comments
* Include the comment body in the subscribe notification

# 1.7.1

## Improvements
* Improve error handling

# 1.7.0

## Features
* Add !subscribe and !unsubscribe commands

## Improvements
* Logging improvements
* Build system improvements

# 1.6.4

## Improvements
* Use https for all outgoing requests
* Improve error handling

# 1.6.3

## Bug Fixes
* Fix an unhandled exception when announcing the host was not found

# 1.6.2

## Bug Fixes
* Fix an issue with unescape underscores in the stats output

# 1.6.1

## Bug Fixes
* Don't reset a command's cooldown when DMing the result

# 1.6.0

## Improvements
* Display aliases and permissions in !help output
* Make !s and !r share output
* Improve error handling
* Don't suggest commands that the commander doesn't have permission to use
* DM the output of a command to the commander if it's on cooldown

## Bug Fixes
* Remove reactions on DMs one at a time to get around permissions issues

# 1.5.0

## Features
* Add support for using reactions to manipulate command output on !leaderboard and !help
* Add black- and white-listing of channels for commands

## Improvements
* Improve error handling
* Delete both command and response if the command is rejected
* Wording/punctuation improvements

# 1.4.0

## Features
* Add !migrate to transfer wins from one player to another
* Add !correct to change the winning comment for a round

## Improvements
* Use the PictureGame API for initialization and leaderboard handling

## Bug Fixes
* Prevent multiple @ characters from causing a here/everyone ping

# 1.3.1

## Features
* Display streak and hiatus information in the round over message

## Improvements
* Revert madlist changes made in 1.2.0
* Make a number of things configurable instead of being hard-coded
* Ignore messages consisting entirely of the command prefix

## Bug Fixes
* Ensure all non-hosts lost the current host role when setting a new host

# 1.3.0

## Features
* Accept messages from the Reddit Bot for updating current round status

## Improvements
* Change !r to alias rank instead of rules

# 1.2.0

## Features
* Change !commands to !help and allow selecting a category

## Improvements
* Logging improvements
* Change madlist commands so that anyone can add/remove themself as mad

# 1.1.1

## Improvements
* Code structure/packaging improvements

## Bug Fixes
* Prevent pinging players when announcing the round title

# 1.1.0

## Features
* Add rate limiting to some commands

## Improvements
* Remove !fox
* Group outputs from !stats and !rank into single messages

## Bug Fixes
* Properly escape underscores in usernames

# 1.0.0

Initial release after taking over development from btwebb

## Features
* Allow !stats to be called without any params, returning the caller's stats
* Add !rank command to fetch the stats of the player at the given rank
* Allow !stats and !rank to be called with multiple parameters
* Add aliases for some commands
* Allow !sethost to work when the parameter is a mention
* Give a suggestion when a command is not found that matches the input
* Add !about to fetch bot version info

## Improvements
* Porting code over to TypeScript and restructure the code into files
* Make usernames passed to !stats case-insensitive
* Prevent the bot from pinging players/roles when copying input to its responses
* Improve logging
* Add variety to the !celebrate command
* Persist the mad list across restarts of the bot
