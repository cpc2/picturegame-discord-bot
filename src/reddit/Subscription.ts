import * as Discord from 'discord.js';
import * as winston from 'winston';

import { saveState } from '../model/PersistableState';
import { State } from '../model/state';

import {
    isNewRound, isSolvedRound,
    CommentEditMessage, CurrentRoundMessage, NewCommentMessage, RedditBotMessageType, RoundStatus,
} from '../reddit/MessageTypes';

import { formatNewRoundSummary } from '../utils/formatter';
import { MessageCharacterLimit } from '../utils/message';
import { cleanName } from '../utils/sanitizer';
import { getUserInfoFromMember } from '../utils/user';

export const CurrentRoundId = '__current';

export type CommentSubscribeMode = 'players' | 'host';

const newCommentsPerRound = new Map<string, NewCommentMessage[]>();
const editedCommentsPerRound = new Map<string, CommentEditMessage[]>();
const awaitingNotifications = new Map<string, Promise<void>>();

function addUser<T>(map: Map<T, Set<string>>, idOrNum: T, userId: string) {
    let players = map.get(idOrNum);
    if (!players) {
        map.set(idOrNum, players = new Set<string>());
    }
    players.add(userId);
}

export async function subscribe(mode: CommentSubscribeMode, user: Discord.User, roundId: string = CurrentRoundId) {
    const state = State.getState();
    const subList = mode === 'players' ? state.data.roundSubscriptions_players : state.data.roundSubscriptions_host;
    addUser(subList, roundId, user.id);
    await saveState(state.data);
}

export async function autoSubscribe(user: Discord.User, username: string) {
    const state = State.getState();
    addUser(state.data.playerSubscriptions, username, user.id);
    await saveState(state.data);
}

function deleteUserOrRound<T>(map: Map<T, Set<string>>, idOrNum: T, userId: string): boolean {
    const players = map.get(idOrNum);
    if (!players?.size) {
        return false;
    }

    players.delete(userId);
    if (!players.size) {
        map.delete(idOrNum);
    }
    return true;
}

export async function unsubscribe(mode: CommentSubscribeMode, user: Discord.User, roundId: string = CurrentRoundId) {
    const state = State.getState();

    const subList = mode === 'players' ? state.data.roundSubscriptions_players : state.data.roundSubscriptions_host;
    const modified = deleteUserOrRound(subList, roundId, user.id);

    if (modified) {
        await saveState(state.data);
    }
}

export async function unAutoSubscribe(user: Discord.User, username: string) {
    const state = State.getState();
    const modified = deleteUserOrRound(state.data.playerSubscriptions, username, user.id);
    if (modified) {
        await saveState(state.data);
    }
}

function deleteUserFromMap(map: Map<any, Set<string>>, userId: string): boolean {
    let updated = false;
    map.forEach((users, key) => {
        if (!users.has(userId)) { return; }

        updated = true;
        users.delete(userId);
        if (users.size === 0) {
            map.delete(key);
        }
    });
    return updated;
}

export async function unsubscribeAll(user: Discord.User | string) {
    if (typeof user !== 'string') {
        user = user.id;
    }

    const state = State.getState();

    let updated = false;
    if (deleteUserFromMap(state.data.roundSubscriptions_players, user)) { updated = true; }
    if (deleteUserFromMap(state.data.playerSubscriptions, user)) { updated = true; }
    if (deleteUserFromMap(state.data.roundSubscriptions_host, user)) { updated = true; }

    if (updated) {
        await saveState(state.data);
    }
}

export async function unsubscribeCurrent() {
    const state = State.getState();

    state.data.roundSubscriptions_players.delete(CurrentRoundId);
    state.data.roundSubscriptions_host.delete(CurrentRoundId);

    await saveState(state.data);
}

export async function addNotifyPlayer(username: string, user: Discord.User) {
    const state = State.getState();
    addUser(state.data.playerNotifications, username.toLowerCase(), user.id);
    await saveState(state.data);
}

export async function removeNotifyPlayer(username: string, user: Discord.User) {
    const state = State.getState();
    const modified = deleteUserOrRound(state.data.playerNotifications, username.toLowerCase(), user.id);
    if (modified) {
        await saveState(state.data);
    }
}

export async function addNotifyRound(roundNumber: number, user: Discord.User) {
    const state = State.getState();
    addUser(state.data.roundNotifications, roundNumber, user.id);
    await saveState(state.data);
}

export async function removeNotifyRound(roundNumber: number, user: Discord.User) {
    const state = State.getState();
    const modified = deleteUserOrRound(state.data.roundNotifications, roundNumber, user.id);
    if (modified) {
        await saveState(state.data);
    }
}

export async function removeAllNotifications(userId: string) {
    const state = State.getState();

    let updated = false;
    if (deleteUserFromMap(state.data.roundNotifications, userId)) { updated = true; }
    if (deleteUserFromMap(state.data.playerNotifications, userId)) { updated = true; }

    if (updated) {
        await saveState(state.data);
    }
}

export async function subscribeCurrentFromNewRound(hostName: string) {
    const state = State.getState();
    const { roundSubscriptions_players: subscriptions, playerSubscriptions: playerSubscriptions } = state.data;

    let updated = false;

    const playerUsers = playerSubscriptions.get(hostName.toLowerCase());
    if (playerUsers?.size) {
        subscriptions.set(CurrentRoundId, new Set(playerUsers));
        updated = true;
    }

    if (updated) {
        await saveState(state.data);
    }
}

export function onNewComment(comment: NewCommentMessage) {
    const state = State.getState();
    const playerSubscriptions = state.data.roundSubscriptions_players;
    const hostSubscriptions = state.data.roundSubscriptions_host;
    const playersToAlert = new Set<string>();

    if (state.data.round.status === 'unsolved' && state.data.round.id === comment.postId) {
        const playersForCurrent = comment.isOp
            ? hostSubscriptions.get(CurrentRoundId) : playerSubscriptions.get(CurrentRoundId);
        playersForCurrent?.forEach(p => playersToAlert.add(p));
    }

    const playersForRound = comment.isOp
        ? hostSubscriptions.get(comment.postId) : playerSubscriptions.get(comment.postId);
    playersForRound?.forEach(p => playersToAlert.add(p));

    if (playersToAlert.size === 0) {
        winston.debug('Received new comment from Reddit bot; nobody to alert', { comment: comment.commentId });
        return Promise.resolve();
    }

    winston.info('Received new comment from Reddit bot', {
        playersToAlert: playersToAlert.size,
        submission: comment.postId,
        comment: comment.commentId,
    });

    let comments = newCommentsPerRound.get(comment.postId);
    if (!comments) {
        newCommentsPerRound.set(comment.postId, comments = []);
    }
    comments.push(comment);

    return enqueueNotifyForComment(comment.postId, playersToAlert);
}

export async function onCommentEdit(comment: CommentEditMessage) {
    const state = State.getState();
    const hostSubscriptions = state.data.roundSubscriptions_host;
    const playersToAlert = new Set<string>();

    if (!comment.isOp) {
        // We only care about OP editing their comments - perhaps to add extra hints
        return;
    }

    if (state.data.round.status === 'unsolved' && state.data.round.id === comment.postId) {
        const playersForCurrent = hostSubscriptions.get(CurrentRoundId);
        playersForCurrent?.forEach(p => playersToAlert.add(p));
    }

    const playersForRound = hostSubscriptions.get(comment.postId);
    playersForRound?.forEach(p => playersToAlert.add(p));

    if (playersToAlert.size === 0) {
        winston.debug('Received edited comment from Reddit bot; nobody to alert', { comment: comment.commentId });
        return;
    }

    winston.info('Received edited comment from Reddit bot', {
        playersToAlert: playersToAlert.size,
        submission: comment.postId,
        comment: comment.commentId,
    });

    let comments = editedCommentsPerRound.get(comment.postId);
    if (!comments) {
        editedCommentsPerRound.set(comment.postId, comments = []);
    }
    comments.push(comment);

    await enqueueNotifyForComment(comment.postId, playersToAlert);
}

function enqueueNotifyForComment(roundId: string, players: Set<string>) {
    const existingPromise = awaitingNotifications.get(roundId);
    if (existingPromise) {
        return existingPromise;
    }

    const newPromise = new Promise<void>((res, rej) => {
        // Delay long enough so that all of the comments from sweep from the reddit bot get grouped
        // into one DM
        setTimeout(() => {
            notifyForComment(roundId, players)
                .then(() => {
                    awaitingNotifications.delete(roundId);
                    res();
                })
                .catch(rej);
        }, 100);
    });
    awaitingNotifications.set(roundId, newPromise);
    return newPromise;
}

async function notifyForComment(roundId: string, players: Set<string>) {
    const state = State.getState();
    const newComments = newCommentsPerRound.get(roundId) ?? [];
    newCommentsPerRound.delete(roundId);
    const editedComments = editedCommentsPerRound.get(roundId) ?? [];
    editedCommentsPerRound.delete(roundId);

    const tasks: Promise<any>[] = [];
    let successCount = 0;
    let failCount = 0;
    let skippedCount = 0;

    winston.debug('Start notifying about comments', {
        numNewComments: newComments.length,
        newComments: newComments.map(c => c.commentId),
        numEditedComments: editedComments.length,
        editedComments: editedComments.map(c => c.commentId),
    });

    const failedUsers = new Set<string>();

    players.forEach(p => {
        const player = state.server.member(p);
        if (!player) { return; }

        function shouldInclude(comment: NewCommentMessage | CommentEditMessage) {
            if (comment.author.toLowerCase() === playerName) {
                return false;
            }

            if (comment.isOp) {
                // Host comments - include root comments, OP replies to themselves, and OP replies to the subscriber
                return comment.parentAuthor === comment.author || comment.parentAuthor?.toLowerCase() === playerName;
            }

            return true;
        }

        const playerName = (player.nickname ?? player.user.username).toLowerCase();
        const filteredNewComments = newComments.filter(shouldInclude);
        const filteredEditedComments = editedComments.filter(shouldInclude);
        if (!filteredNewComments.length && !filteredEditedComments.length) {
            skippedCount++;
            return;
        }

        const send = (message: string) => {
            return player.send(message)
                .then(() => { successCount++; })
                .catch(e => {
                    failCount++;
                    winston.warn('Unable to notify player; unsubscribing them from future notifications', {
                        ...getUserInfoFromMember(player),
                        error: e,
                    });
                    failedUsers.add(player.id);
                });
        };

        if (filteredNewComments.length) {
            tasks.push(send(getCommentsMessage(filteredNewComments, roundId, playerName)));
        }
        if (filteredEditedComments.length) {
            tasks.push(send(getCommentsMessage(filteredEditedComments, roundId, playerName)));
        }
    });

    await Promise.all(tasks);
    winston.info('Done notifying about comments', {
        successCount,
        failCount,
        skippedCount,
        numNewComments: newComments.length,
        newComments: newComments.map(c => c.commentId),
        numEditedComments: editedComments.length,
        editedComments: editedComments.map(c => c.commentId),
    });

    if (failedUsers.size > 0) {
        for (const userId of failedUsers) {
            await unsubscribeAll(userId);
        }

        winston.info('Unsubscribed users from all future notifications', { numUsers: failedUsers.size });
    }
}

function getCommentsMessage(comments: NewCommentMessage[] | CommentEditMessage[], roundId: string, playerName: string) {
    if (comments.length === 1) {
        return getSingleCommentMessage(comments[0], roundId, playerName);
    }

    const isEdit = comments[0].type === RedditBotMessageType.CommentEdit;
    let message = isEdit
        ? `${comments.length} comments have been edited at <https://redd.it/${roundId}>:\n\n`
        : `${comments.length} new comments have been posted at <https://redd.it/${roundId}>:\n\n`;

    for (const comment of comments) {
        message += `${getCommentUrl(comment, roundId)} by ${comment.author}:\n`;
        const messageWithComment = message + comment.body + '\n\n';
        if (messageWithComment.length > MessageCharacterLimit) {
            message += '*Comment too long to show here*\n\n';
        } else {
            message = messageWithComment;
        }
    }
    return message;
}

function getSingleCommentMessage(comment: NewCommentMessage | CommentEditMessage, roundId: string, playerName: string) {
    let message: string;
    if (comment.type === RedditBotMessageType.CommentEdit) {
        message = `${comment.author} edited their comment at ${getCommentUrl(comment, roundId)}:\n\n`;
    } else if (comment.parentAuthor?.toLowerCase() === playerName) {
        message = `${comment.author} replied to your comment at ${getCommentUrl(comment, roundId)}:\n\n`;
    } else {
        message = `A new comment has been posted by ${comment.author} at ${getCommentUrl(comment, roundId)}:\n\n`;
    }

    if (message.length + comment.body.length > MessageCharacterLimit) {
        message += '*Comment too long to show here*';
    } else {
        message += comment.body;
    }
    return message;
}

function getCommentUrl(comment: NewCommentMessage | CommentEditMessage, roundId: string) {
    return `<https://reddit.com/comments/${roundId}/-/${comment.commentId}>`;
}

function getRoundNotificationMessage(message: CurrentRoundMessage, notificationMode: 'round' | 'player') {
    if (isNewRound(message)) {
        if (notificationMode === 'round') {
            return `Round ${message.roundNumber} has been posted:\n\n` + formatNewRoundSummary(message);
        }
        return `**${cleanName(message.host)}** has posted a new round:\n\n` + formatNewRoundSummary(message);
    }

    const currentRound = State.getState().data.round;
    if (isSolvedRound(message)) {
        return `Round ${currentRound.roundNumber} has been solved by **${cleanName(message.winner)}**.\n\n` +
            `Winning comment: <https://reddit.com/comments/${currentRound.id}/-/${message.commentId}>`;
    }

    return `Round ${currentRound.roundNumber} has been ${message.status}.`;
}

export async function onRoundStatus(message: CurrentRoundMessage) {
    const promises: Promise<any>[] = [];

    const state = State.getState();
    const currentRound = state.data.round;
    const roundNumber = isNewRound(message) ? message.roundNumber : currentRound.roundNumber;
    const host = isNewRound(message) ? message.host : currentRound.host;

    if (!roundNumber || !host) {
        return;
    }

    let successCount = 0;
    let failCount = 0;
    const failedUsers = new Set<string>();

    const usersForRound = state.data.roundNotifications.get(roundNumber);
    const usersForHost = state.data.playerNotifications.get(host.toLowerCase());

    const usersToNotify = new Map<string, 'round' | 'player'>();
    for (const user of usersForRound ?? new Set<string>()) {
        usersToNotify.set(user, 'round');
    }
    for (const user of usersForHost ?? new Set<string>()) {
        if (!usersToNotify.has(user)) {
            usersToNotify.set(user, 'player');
        }
    }

    for (const [userId, reason] of usersToNotify) {
        const user = state.server.member(userId);
        if (!user) { continue; }

        promises.push(user.send(getRoundNotificationMessage(message, reason))
            .then(() => { successCount++; })
            .catch(e => {
                failCount++;
                winston.warn('Unable to notify player; unsubscribing them from future notifications', {
                    ...getUserInfoFromMember(user),
                    error: e,
                });
                failedUsers.add(user.id);
            }));
    }

    await Promise.all(promises);
    winston.info('Done notifying about round status', { successCount, failCount });

    if (failedUsers.size > 0) {
        for (const userId of failedUsers) {
            await removeAllNotifications(userId);
        }
    }

    winston.info('Unsubscribed users from all future notifications', { numUsers: failedUsers.size });

    if (usersForRound && (message.status === RoundStatus.Abandoned || message.status === RoundStatus.Solved)) {
        state.data.roundNotifications.delete(roundNumber);
        await saveState(state.data);
    }
}
