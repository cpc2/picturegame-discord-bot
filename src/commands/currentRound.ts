import * as Bot from '../model/bot';
import { State } from '../model/state';

import * as utils from '../utils';

import * as config from '../../config.json';

export const currentRoundCommands: Bot.CommandSpec = {
    description: 'Current Round & Host',
    commands: [
        {
            command: 'current',
            description: `Returns the latest round on /r/${config.reddit.subreddit}`,
            execute: () => async send => {
                const roundData = State.getState().data.round;
                if (roundData.status === 'unsolved') {
                    const timeSinceStart = roundData.timeStarted
                        && utils.formatTimeDifference(roundData.timeStarted, new Date());
                    const header = timeSinceStart
                        ? `The current round has been up for ${timeSinceStart}:\n\n`
                        : 'The current round is:\n\n';

                    await send(header +
                        `**${roundData.title}** by ${utils.cleanName(roundData.host)}\n` +
                        `<https://redd.it/${roundData.id}>\n` +
                        `${roundData.pic}`);

                } else if (roundData.status === 'over') {
                    const timeSinceSolve = roundData.timeSolved
                        ? utils.formatTimeDifference(roundData.timeSolved, new Date())
                        : 'some time';

                    await send(`The latest round was solved ${timeSinceSolve} ago by ${roundData.winner}.\n\n` +
                        `<https://redd.it/${roundData.id}>`);

                } else if (roundData.status === 'deleted') {
                    await send('The latest round was deleted.\n\n' +
                        `<https://redd.it/${roundData.id}>`);

                } else if (roundData.status === 'abandoned') {
                    await send('The latest round was abandoned.\n\n' +
                        `<https://redd.it/${roundData.id}>`);

                } else {
                    await send('The current round has an error.\n\n' +
                        `<https://redd.it/${roundData.id}>`);
                }
            },
        },
        {
            command: 'sethost',
            description: `Sets a user as the host of the current round on /r/${config.reddit.subreddit}`,
            parameters: ['user'],
            maxParameters: 1,
            execute: (_m, username) => async send => {
                const usernameMatch = username.match(utils.memberPattern);
                if (usernameMatch) {
                    const member = State.getState().server.members.cache.get(usernameMatch[1]);
                    if (member) {
                        await utils.setCurrentHostByMember(member);
                        return;
                    }
                }

                username = utils.sanitizeWord(username);
                await utils.replaceCurrentHostByName(username, send);
            },
        },
        {
            command: 'clearhost',
            description: 'Clears the current host',
            execute: () => utils.clearCurrentHost,
        },
    ],
};
