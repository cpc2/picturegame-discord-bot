import * as Bot from '../model/bot';
import { saveState } from '../model/PersistableState';
import { State } from '../model/state';
import * as utils from '../utils';

export const madlistCommands: Bot.CommandSpec = {
    description: 'Mad list',
    commands: [
        {
            command: 'whoismad',
            description: 'Returns the list of everyone who is mad today',
            execute: () => async send => {
                const madList = State.getState().data.madList;

                if (madList.length === 0) {
                    await send('Nobody is mad today!');
                    return;
                }
                await send(`Here's who's mad today:\n\n${madList.join('\n')}`);
            },
        },
        {
            command: 'addmad',
            description: 'Adds users to the list of people who are mad today',
            parameters: ['...users'],
            execute: (_, ...names) => async () => {
                const madList = State.getState().data.madList;
                madList.push(...names.map(utils.sanitizeWord));
                await saveMadList(madList);
            },
        },
        {
            command: 'removemad',
            description: 'Removes users from the list of people who are mad today',
            parameters: ['...users'],
            execute: (_, ...names) => async () => {
                const namesSet = new Set<string>(names.map(name => utils.sanitizeWord(name).toLowerCase()));

                await saveMadList(State.getState().data.madList.filter(p => !namesSet.has(p.toLowerCase())));
            },
        },
        {
            command: 'clearmad',
            description: 'Clears the list of people who are mad today',
            execute: () => async send => {
                await saveMadList([]);
                await send('Nobody is mad any more!');
            },
        },
    ],
};

function saveMadList(madList: string[]) {
    const state = State.getState();
    state.data.madList = madList;
    state.resetCooldowns('whoismad');
    return saveState(state.data);
}
