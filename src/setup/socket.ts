import * as winston from 'winston';

import { State } from '../model/state';

import { onReceiveMessage } from '../reddit/RedditBot';
import { logError } from '../utils/error';
import { tryOrLog } from '../utils/promise';

import * as config from '../../config.json';

export function setupSocket() {
    const state = State.getState();

    state.socket.on('error', err => {
        logError(err, 'UDP socket error');
    });

    state.socket.on('message', msg => {
        tryOrLog(onReceiveMessage(JSON.parse(msg.toString('utf-8'))), 'Error handling UDP message');
    });

    state.socket.on('listening', () => {
        const address = state.socket.address();
        const addressDesc = typeof address === 'string' ? address : `${address.address}:${address.port}`;
        winston.info(`UDP server listening on ${addressDesc}`);
    });

    state.socket.bind(config.bot.port, config.bot.address);
}
