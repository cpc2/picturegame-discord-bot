import * as winston from 'winston';

import * as Bot from '../model/bot';
import { State } from '../model/state';

import { tryOrLog } from '../utils/promise';
import { replaceCurrentHostByName, setDescription } from '../utils/rounds';

export async function setupData() {
    const state = State.getState();

    const apiRound = await tryOrLog(state.api.getCurrent(), 'Failed to get current round');

    const id = apiRound?.id;
    const round: Bot.RoundData = {
        id,
    };

    if (apiRound) {
        round.host = apiRound.hostName;
        round.pic = apiRound.postUrl;
        round.timeStarted = apiRound.postTime === undefined ? undefined : new Date(apiRound.postTime * 1000);
        round.timeSolved = apiRound.winTime === undefined ? undefined : new Date(apiRound.winTime * 1000);
        round.winner = apiRound.winnerName;
        round.roundNumber = apiRound.roundNumber;
        round.status = apiRound.winTime === undefined ? 'unsolved' : 'over';
        round.title = apiRound.title;
    }

    state.data.round = round;
    winston.info('Current round set!');

    const promises: Promise<any>[] = [
        state.updateLeaderboard(),
    ];

    if (id) {
        promises.push(setDescription(id));
    }
    if (round.host) {
        promises.push(replaceCurrentHostByName(round.host as string));
    }

    await Promise.all(promises);
}
