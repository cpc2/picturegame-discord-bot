import { State } from '../model/state';

export const memberPattern = /<@!?(\d+)>/;
export const rolePattern = /<@&(\d+)>/;
export const channelPattern = /<#(\d+)>/;

export const redditUsernamePattern = /^[A-Za-z0-9-_]{1,20}$/;

/** Converts any pings to plaintext. */
export function sanitizeWord(word: string): string {
    if (!word) {
        return '';
    }

    const server = State.getState().server;

    word = word.replace(/@+everyone/g, 'everyone').replace(/@+here/g, 'here');

    let playerMatch = word.match(memberPattern);
    while (playerMatch) {
        const player = server.members.cache.get(playerMatch[1]);
        const name = player ? (player.nickname ?? player.user.username) : '';
        word = word.replace(memberPattern, name);
        playerMatch = word.match(memberPattern);
    }

    let roleMatch = word.match(rolePattern);
    while (roleMatch) {
        const role = server.roles.cache.get(roleMatch[1]);
        const name = role ? role.name : '';
        word = word.replace(rolePattern, name);
        roleMatch = word.match(rolePattern);
    }

    let channelMatch = word.match(channelPattern);
    while (channelMatch) {
        const channel = server.channels.cache.get(channelMatch[1]);
        const name = channel ? channel.name : '';
        word = word.replace(channelPattern, name);
        channelMatch = word.match(channelPattern);
    }

    return word;
}

export function cleanName(word?: string): string {
    return word?.replace(/[^\w- ]/g, '').replace(/_/g, '\\_') ?? '';
}
