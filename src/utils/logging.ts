import * as Discord from 'discord.js';
import * as winston from 'winston';

import { sendLongMessage } from './message';

import { State } from '../model/state';

import * as config from '../../config.json';

export enum LogLevel {
    Debug = 'debug',
    Info = 'info',
    Warn = 'warn',
    Error = 'error',
    Fatal = 'fatal',
    Critical = 'critical',
}
export const LogLevelOrdinal: ReadonlyArray<string>
    = [LogLevel.Debug, LogLevel.Info, LogLevel.Warn, LogLevel.Error, LogLevel.Fatal, LogLevel.Critical];

export async function writeLogToDiscord(serviceName: string, level: string, message: string, data?: any) {
    const logHandlers = config.bot.log.channelLogging;
    if (!logHandlers) { return; }

    const logOrdinal = LogLevelOrdinal.indexOf(level.toLowerCase());
    const state = State.getState();

    for (const handler of logHandlers) {
        if (handler.serviceName === serviceName
            && LogLevelOrdinal.indexOf(handler.level) <= logOrdinal) {

            const channel = state.server.channels.cache.get(handler.channelId);
            if (!channel) {
                winston.warn('Channel not found for log message', { channelId: handler.channelId, skipDiscord: true });
                continue;
            }

            let text = `**${serviceName} - ${level.toUpperCase()}**\n${message}`;
            if (data) {
                text += '\n' + JSON.stringify(data);
            }
            await sendLongMessage(channel as Discord.TextChannel, text);
        }
    }
}
