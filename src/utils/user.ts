import * as Discord from 'discord.js';

export interface UserInfo {
    nickname?: string | null;
    username: string;
    userId: string;
    discriminator: string;
}

export function getUserInfoFromMessage(message: Discord.Message): UserInfo {
    return message.member ? getUserInfoFromMember(message.member) : getUserInfoFromUser(message.author);
}

export function getUserInfoFromMember(member: Discord.GuildMember): UserInfo {
    return {
        nickname: member.nickname,
        ...getUserInfoFromUser(member.user),
    };
}

export function getUserInfoFromUser(user: Discord.User): UserInfo {
    return {
        username: user.username,
        discriminator: user.discriminator,
        userId: user.id,
    };
}
