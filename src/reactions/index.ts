import { register } from '../model/reactions';

import { helpReactionHandler } from './help';
import { leaderboardReactionHandler } from './leaderboard';

export function setupReactionHandlers() {
    register(
        helpReactionHandler,
        leaderboardReactionHandler,
    );
}
