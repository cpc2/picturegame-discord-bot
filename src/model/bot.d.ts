import * as Discord from 'discord.js';

import { ReactionKey } from './ReactionKey';

export type RoundStatusFlair = 'unsolved' | 'over' | 'abandoned' | 'deleted';

export interface RoundData {
    status?: RoundStatusFlair;
    roundNumber?: number;
    title?: string;
    host?: string;
    winner?: string;
    id?: string;
    pic?: string;
    timeStarted?: Date;
    timeSolved?: Date;
}

export interface Player {
    name: string;
    winCount: number;
    rank: number;
}

export interface CommandSpec {
    description: string;
    guideUrl?: string;
    commands: Command[];
}

export interface Command {
    command: string;
    description: string;
    parameters?: string[];
    permissions?: Set<string>;
    aliases?: string[];
    minParameters?: number;
    maxParameters?: number;
    cooldown?: number; // In minutes
    allowedChannels?: Set<string>;
    forbiddenChannels?: Set<string>;
    reply?: boolean;
    execute: (message: Discord.Message, ...params: string[]) => (send: SendMessageCallback) => Promise<void>;
}

export interface SendMessageCallback {
    (body: string): Promise<Discord.Message | Discord.Message[] | void>;
    (options: Discord.MessageOptions): Promise<Discord.Message | Discord.Message[] | void>;
}

export interface DataState extends PersistableState {
    leaderboard: Player[];
    players: Map<string, Player>;
    round: RoundData;
    commands: string[];
    commandCooldowns: Map<string, CooldownState>;
    customCooldownResponses: Map<string, string>;
    recentStats: Map<string, RecentCommands<Player>>;

    reactableMessages: Map<ReactionKey, ReactableMessage<any>>;
    messageTypes: Map<string, string>;
}

export interface PersistableState {
    madList: string[];

    /** Subscriptions to players' comments on existing threads, by reddit thread id */
    roundSubscriptions_players: Map<string, Set<string>>;

    /** Subscriptions to hosts' comments on existing threads, by reddit thread id */
    roundSubscriptions_host: Map<string, Set<string>>;

    /**
     * Auto-subscriptions to players' comments on rounds by reddit users, by username.
     * Will get converted to a __current subscription whenever this user posts a round
     */
    playerSubscriptions: Map<string, Set<string>>;

    /** Subscriptions to round start- and end- notifications for specific rounds */
    roundNotifications: Map<number, Set<string>>;

    /** Subscriptions to round start- and end- notifications for rounds by specific players */
    playerNotifications: Map<string, Set<string>>;
}

export interface ReactableMessage<T> {
    messageId: string;
    userId: string;
    state: T;
    type: string;
}

export interface ReactionHandler<T> {
    type: string;
    emojis: string[] | (() => string[]);
    init: (...args: any[]) => T;
    update: (state: T, reaction: Discord.Emoji | Discord.ReactionEmoji) => boolean;
    getContents: (state: T) => string;
}

/**
 * Encompasses recently requested commands in a channel
 * Used to compile subsequent commands calls into one message to reduce spam
 * Compile up to a given timeout, or a maximum number of entries, whichever happens first
 */
export interface RecentCommands<T> {
    message: Discord.Message;
    timer: NodeJS.Timer;
    queries: Set<T>;
}

export interface CooldownState {
    [channelId: string]: Date;
}
