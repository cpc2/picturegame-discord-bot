import * as discord from 'discord.js';

export interface ReactionKey extends String {
    _dummyReactionKeyVar: any;
}

export function toReactionKey(channel: discord.Channel, command: string): ReactionKey {
    return `${channel.id}|${command}` as any as ReactionKey;
}
